FROM elixir:1.9.0-alpine

RUN \
    mkdir -p /opt/app && \
    chmod -R 777 /opt/app && \
    apk update && \
    apk --no-cache --update add \
      git make g++ wget curl inotify-tools \
      nodejs nodejs-npm && \
    npm install npm -g --no-progress && \
    update-ca-certificates --fresh && \
    rm -rf /var/cache/apk/*

ENV PATH=./node_modules/.bin:$PATH \
    MIX_HOME=/opt/mix \
    HEX_HOME=/opt/hex \
    HOME=/opt/app

RUN mix local.hex --force && \
    mix local.rebar --force

RUN mix archive.install --force hex phx_new 1.4.9

WORKDIR /opt/app

CMD ["/bin/sh"]

FROM golang:1.12.7-alpine3.10

RUN \
    mkdir -p /opt/app && \
    chmod -R 777 /opt/app && \
    apk update && \
    apk --no-cache --update add \
      git make g++ wget curl inotify-tools \
      nodejs nodejs-npm && \
    npm install npm -g --no-progress && \
    update-ca-certificates --fresh && \
    rm -rf /var/cache/apk/*

ENV PATH=./node_modules/.bin:$PATH \
    HOME=/opt/app

RUN go get -u -v github.com/gobuffalo/buffalo/buffalo

WORKDIR /opt/app

CMD ["/bin/sh"]
